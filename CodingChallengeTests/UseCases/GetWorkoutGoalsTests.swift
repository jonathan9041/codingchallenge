//
//  GetWorkoutGoalsTests.swift
//  CodingChallengeTests
//
//  Created by Jonathand Alberto Serrano Serrano on 3/25/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import XCTest
@testable import CodingChallenge

final class GetWorkoutGoalsTests: XCTestCase {
    typealias `Self` = GetWorkoutGoalsTests
    struct MockError: Error {}
    
    var workoutGoalCache: MockWorkoutGoalCacheService!
    var workoutGoalsService: MockWorkoutGoalsService!
    var reachabilityService: MockReachabilityService!
    
    static let mockWorkoutGoals: [WorkoutGoal] = [.init(id: "mock_id1",
                                                              title: "mock_title1",
                                                              description: "mock_description_1",
                                                              type: "mock_type",
                                                              goal: 10,
                                                              reward: .init(trophy: "mock_tropy",
                                                                            points: 1)),
                                                  .init(id: "mock_id2",
                                                        title: "mock_title2",
                                                        description: "mock_description2",
                                                        type: "mock_type",
                                                        goal: 20,
                                                        reward: .init(trophy: "mock_tropy",
                                                                      points: 2))]
    
    var sut: GetWorkoutGoalsAdapter!
    
    override func setUp() {
        super.setUp()
        workoutGoalCache = MockWorkoutGoalCacheService()
        workoutGoalsService = MockWorkoutGoalsService()
        reachabilityService = MockReachabilityService()
        
        sut = GetWorkoutGoalsAdapter(dependencies: .init(workoutGoalsService: workoutGoalsService,
                                                         workoutGoalCache: workoutGoalCache,
                                                         reachabilityService: reachabilityService))
    }
    
    override func tearDown() {
        sut = nil
        workoutGoalCache = nil
        workoutGoalsService = nil
        super.tearDown()
    }
    
    func test_ThereIsInternetConnectionAndWorkoutGoalsToRetrieve_Execute_ServiceDependencyResponseIsSuccessful() {
        // Given
        let workoutGoalsExpected = Self.mockWorkoutGoals
        var workoutGoalsResponse: ServiceResponse<[WorkoutGoal]>?
        reachabilityService.mockIsConnectedToInternet(value: true)
        workoutGoalsService.mockWorkoutGoalsResponse = .success(response: workoutGoalsExpected)
        
        // When
        sut.execute { response in
            workoutGoalsResponse = response
        }
        
        // Then
        guard case .success(let workoutGoals) = workoutGoalsResponse else {
            XCTFail("The workoutgoals must be returned because the workout goals response was successful")
            return
        }
        
        XCTAssertTrue(reachabilityService.getIsConnectedToInternetInvocationCounter() > 0,
                      "The method isConnectedToInternet must be called because this validation is executed before to call the workout goals service")
        XCTAssertTrue(workoutGoalsService.getWorkoutGoalsInvocationCounter() > 0,
                      "The method getWorkoutGoals must be called because this is the service method to retrieve the workout goals.")
        XCTAssertTrue(workoutGoalCache.getSaveWorkoutGoalsInvocationsCounter() > 0,
                      "The method saveWorkoutGoals must be called because the workout goals are saved when a new goals are retrieved")
        XCTAssertTrue(workoutGoalCache.getRetrieveWorkoutGoalInvocationsCounter() == 0,
                      "The method retrieveWorkoutGoals must not be called because there is internet connection")
        XCTAssertEqual(workoutGoals,
                       workoutGoalsExpected,
                       "The workoutGoals must be equal to workoutGoalsExpected because it was the value given by the dependency service")
    }
    
    func test_WorkoutGoalsServiceFails_Execute_ServiceReturnsAFailureResponse() {
        // Given
        let mockError = MockError()
        var workoutGoalsResponse: ServiceResponse<[WorkoutGoal]>?
        reachabilityService.mockIsConnectedToInternet(value: true)
        workoutGoalsService.mockWorkoutGoalsResponse = .failure(error: mockError)
        
        // When
        sut.execute { response in
            workoutGoalsResponse = response
        }
        
        // Then
        guard case .failure = workoutGoalsResponse else {
            XCTFail("The workoutgoals must be returned because the workout goals response was successful")
            return
        }
        
        XCTAssertTrue(reachabilityService.getIsConnectedToInternetInvocationCounter() > 0,
                      "The method isConnectedToInternet must be called because this validation is executed before to call the workout goals service")
        XCTAssertTrue(workoutGoalsService.getWorkoutGoalsInvocationCounter() > 0,
                      "The method getWorkoutGoals must called because this is the service method to retrieve the workout goals.")
        XCTAssertTrue(workoutGoalCache.getSaveWorkoutGoalsInvocationsCounter() == 0,
                      "The method saveWorkoutGoals must not be called because the workout goals service failed")
        XCTAssertTrue(workoutGoalCache.getRetrieveWorkoutGoalInvocationsCounter() == 0,
                      "The method retrieveWorkoutGoals must not be called because there is internet connection")
    }
    
    func test_ThereIsNoInternetConnectionAndWorkoutGoalsStored_Execute_CacheServiceRetrieveTheWorkoutGoals() {
        // Given
        let workoutGoalsExpected = Self.mockWorkoutGoals
        var workoutGoalsResponse: ServiceResponse<[WorkoutGoal]>?
        reachabilityService.mockIsConnectedToInternet(value: false)
        workoutGoalCache.mockRetrieveWorkoutGoals = workoutGoalsExpected
        
        // When
        sut.execute { response in
            workoutGoalsResponse = response
        }
        
        // Then
        guard case .success(let workoutGoals) = workoutGoalsResponse else {
            XCTFail("The workoutgoals must be returned because the workout goals response was successful")
            return
        }
        
        XCTAssertTrue(reachabilityService.getIsConnectedToInternetInvocationCounter() > 0,
                      "The method isConnectedToInternet must be called because this validation is executed before to call the workout goals service")
        XCTAssertTrue(workoutGoalCache.getRetrieveWorkoutGoalInvocationsCounter() > 0,
                      "The method getRetrieveWorkoutGoals must be called because this is the cache service method to retrieve the workout goals.")
        XCTAssertTrue(workoutGoalCache.getSaveWorkoutGoalsInvocationsCounter() == 0,
                      "The method saveWorkoutGoals must not be called because the workout goals are retrieve from cache")
        XCTAssertTrue(workoutGoalsService.getWorkoutGoalsInvocationCounter() == 0,
                      "The method getWorkoutGoals must not be called because there is no internet connection")
        XCTAssertEqual(workoutGoals,
                       workoutGoalsExpected,
                       "The workoutGoals must be equal to workoutGoalsExpected because it was the value given by the dependency service")
    }
    
    func test_ThereIsNoInternetConnectionAndThereIsNoWorkoutGoalsStored_Execute_CacheServiceReturnsNil() {
        // Given
        var workoutGoalsResponse: ServiceResponse<[WorkoutGoal]>?
        reachabilityService.mockIsConnectedToInternet(value: false)
        workoutGoalCache.mockRetrieveWorkoutGoals = nil
        
        // When
        sut.execute { response in
            workoutGoalsResponse = response
        }
        
        // Then
        guard case .failure = workoutGoalsResponse else {
            XCTFail("the response must be equal to failure because there is no workout goals stored")
            return
        }
        
        XCTAssertTrue(reachabilityService.getIsConnectedToInternetInvocationCounter() > 0,
                      "The method isConnectedToInternet must be called because this validation is executed before to call the workout goals service")
        XCTAssertTrue(workoutGoalCache.getRetrieveWorkoutGoalInvocationsCounter() > 0,
                      "The method getRetrieveWorkoutGoals must be called because this is the cache service method to retrieve the workout goals.")
        XCTAssertTrue(workoutGoalCache.getSaveWorkoutGoalsInvocationsCounter() == 0,
                      "The method saveWorkoutGoals must not be called because the workout goals are retrieve from cache")
        XCTAssertTrue(workoutGoalsService.getWorkoutGoalsInvocationCounter() == 0,
                      "The method getWorkoutGoals must not be called because there is no internet connection")
    }
}

//
//  MockWorkoutGoalCacheService.swift
//  CodingChallengeTests
//
//  Created by Jonathand Alberto Serrano Serrano on 3/25/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
@testable import CodingChallenge

final class MockWorkoutGoalCacheService: WorkoutGoalCacheService {
    private var saveWorkoutGoalsInvocationsCounter: Int = 0
    private var retrieveWorkoutGoalInvocationsCounter: Int = 0
    
    var mockRetrieveWorkoutGoals: [WorkoutGoal]?
    
    func saveWorkoutGoals(workoutGoals: [WorkoutGoal]) {
        saveWorkoutGoalsInvocationsCounter += 1
    }
    
    func retrieveWorkoutGoals() -> [WorkoutGoal]? {
        retrieveWorkoutGoalInvocationsCounter += 1
        return mockRetrieveWorkoutGoals
    }
    
    func getSaveWorkoutGoalsInvocationsCounter() -> Int {
        return saveWorkoutGoalsInvocationsCounter
    }
    
    func getRetrieveWorkoutGoalInvocationsCounter() -> Int {
        return retrieveWorkoutGoalInvocationsCounter
    }
}

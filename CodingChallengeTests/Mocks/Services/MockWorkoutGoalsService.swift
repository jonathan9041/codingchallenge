//
//  MockWorkoutGoalsService.swift
//  CodingChallengeTests
//
//  Created by Jonathand Alberto Serrano Serrano on 3/25/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
@testable import CodingChallenge

final class MockWorkoutGoalsService: WorkoutGoalsService {
    struct MockError: Error {}
    
    var workoutGoalsInvocationCounter: Int = 0
    var mockWorkoutGoalsResponse: ServiceResponse<[WorkoutGoal]> = .failure(error: MockError())
    
    func getWorkoutGoals(completion: @escaping WorkoutGoalsResponse) {
        workoutGoalsInvocationCounter += 1
        completion(mockWorkoutGoalsResponse)
    }
    
    func getWorkoutGoalsInvocationCounter() -> Int {
        return workoutGoalsInvocationCounter
    }
}

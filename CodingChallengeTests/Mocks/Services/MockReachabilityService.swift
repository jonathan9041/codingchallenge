//
//  MockReachabilityService.swift
//  CodingChallengeTests
//
//  Created by Jonathand Alberto Serrano Serrano on 3/25/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
@testable import CodingChallenge

final class MockReachabilityService: ReachabilityService {
    private var mockIsconnectedToInternet: Bool = false
    private var isConnectedToInternetInvocationCounter: Int = 0
    
    var isConnectedToInternet: Bool {
        return mockIsconnectedToInternet
    }
    
    func mockIsConnectedToInternet(value: Bool) {
        mockIsconnectedToInternet = value
        isConnectedToInternetInvocationCounter += 1
    }
    
    func getIsConnectedToInternetInvocationCounter() -> Int {
        return isConnectedToInternetInvocationCounter
    }
}

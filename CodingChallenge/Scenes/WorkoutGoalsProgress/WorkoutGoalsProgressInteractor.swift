//
//  WorkoutGoalsProgressInteractor.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
import UIKit

protocol WorkoutGoalsProgressInteractorProtocol {
    func handleSceneDidLoad()
    func handleSceneWillAppear()
}

final class WorkoutGoalsProgressInteractor: WorkoutGoalsProgressInteractorProtocol{
    struct Dependencies {
        let requestHealthInformationAccess: RequestHealthInformationAccess = RequestHealthInformationAccessAdapter()
        let getWorkoutGoalsProgress: GetWorkoutsGoalsProgress = GetWorkoutGoalsProgressAdapter()
        let getDailyHeartRateAverage: GetDailyHeartRateAverage = GetDailyHeartRateAverageAdapter()
        let getWorkoutGoals: GetWorkoutGoals = GetWorkoutGoalsAdapter()
        let notificationCenter: NotificationCenter = .default
    }
    
    let dependencies: Dependencies
    var presenter: WorkoutGoalsProgressPresenterProtocol?
    var authorized: Bool = false
    
    private var appStateObserver: AnyObject?
    
    init(depedencies: Dependencies = .init()) {
        self.dependencies = depedencies
        observeAppOnTheForeground()
    }
    
    deinit {
        guard let observer = appStateObserver else { return }
        dependencies.notificationCenter.removeObserver(observer)
    }
    
    func handleSceneWillAppear() {
        guard authorized else {
            return
        }
        
        refreshWorkoutGoalsProgress()
        refreshDailyHeartRateAverage()
    }
       
    func handleSceneDidLoad() {
        requestHealthInformationAccess()
    }
}

private extension WorkoutGoalsProgressInteractor {
    func observeAppOnTheForeground() {
        appStateObserver = dependencies.notificationCenter.addObserver(forName: UIApplication.willEnterForegroundNotification,
                                                    object: nil,
                                                    queue: nil) { [weak self] _ in
                                                        guard
                                                            let strongSelf = self,
                                                            strongSelf.authorized
                                                        else {
                                                            return
                                                        }
                                                        strongSelf.refreshWorkoutGoalsProgress()
                                                        strongSelf.refreshDailyHeartRateAverage()
        }
    }
    func refreshWorkoutGoalsProgress() {
        dependencies.getWorkoutGoalsProgress.execute { [weak self] (workoutGoalsProgress, error) in
            guard let strongSelf = self else {
                return
            }
            
            guard let workoutGoalsProgress = workoutGoalsProgress else {
                let errorDescription = error.compactMap { $0?.localizedDescription }.joined(separator: ",")
                self?.presenter?.formatErrorAlert(message: errorDescription == "" ? "Error getting workout goals progress" : errorDescription, action: nil)
                return
            }
            
            strongSelf.dependencies.getWorkoutGoals.execute(completion: { (workoutGoalsResponse) in
                switch workoutGoalsResponse {
                case .success(let workoutGoals):
                    strongSelf.presenter?.formatWorkoutGoalsProgress(with: workoutGoalsProgress, workoutGoals: workoutGoals)
                case .failure(let workoutGoalsError):
                    self?.presenter?.formatErrorAlert(message: "Error getting workout goals: \(workoutGoalsError.localizedDescription)", action: nil)
                }
            })
        }
    }
    
    func refreshDailyHeartRateAverage() {
        dependencies.getDailyHeartRateAverage.execute { [weak self] (heartRateData, error) in
            guard
                let heartRateData = heartRateData,
                error == nil
            else {
                self?.presenter?.formatErrorAlert(message: error?.localizedDescription ?? "There was an eror getting the daily heart rate average", action: nil)
                return
            }
            
            self?.presenter?.formatDailyHeartRate(with: heartRateData)
        }
    }
    
    func requestHealthInformationAccess() {
        dependencies.requestHealthInformationAccess.execute { [weak self] (authorized, error) in
            guard error == nil else {
                self?.presenter?.formatErrorAlert(message: "Error with HealthInformationAccess. \(error?.localizedDescription ?? "")", action: nil)
                return
            }
            
            guard authorized else {
                self?.presenter?.formatErrorAlert(message: "Health information Access Error. Unauthorized access", action: nil)
                return
            }
            
            self?.authorized = true
            self?.refreshWorkoutGoalsProgress()
            self?.refreshDailyHeartRateAverage()
        }
    }
}

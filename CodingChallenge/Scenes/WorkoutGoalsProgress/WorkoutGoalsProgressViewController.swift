//
//  WorkoutGoalsProgressViewController.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit

protocol WorkoutGoalsProgressViewControllerProtocol {
    func updateView(with viewModel: WorkoutGoalsProgressViewModel)
    func displayErrorAlert(title: String, message: String, action: (() -> Void)?)
    func showDailyHeartRateAverage(with heartRateViewModel: HeartRateVieModel)
}

class WorkoutGoalsProgressViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heartRateLabel: UILabel!
    @IBOutlet weak var heartRateImage: UIImageView!
    
    var interactor: WorkoutGoalsProgressInteractorProtocol?
    var viewModel: WorkoutGoalsProgressViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        interactor?.handleSceneDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.handleSceneWillAppear()
    }
}

extension WorkoutGoalsProgressViewController: WorkoutGoalsProgressViewControllerProtocol {
    func showDailyHeartRateAverage(with viewModel: HeartRateVieModel) {
        heartRateLabel.text = viewModel.heartRateText
        heartRateImage.image = viewModel.heartRateimage        
    }
    
    func displayErrorAlert(title: String, message: String, action: (() -> Void)?) {
        displayAlert(title: title, message: message, acceptButtonText: "Accept", completion: action)
    }
    
    func updateView(with viewModel: WorkoutGoalsProgressViewModel) {
        self.viewModel = viewModel
        
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

private extension WorkoutGoalsProgressViewController{
    func setup() {
        let interactor = WorkoutGoalsProgressInteractor()
        let presenter = WorkoutGoalsProgressPresenter()
        presenter.view = self
        interactor.presenter = presenter
        self.interactor = interactor
        configTableView()
    }
    
    func configTableView() {
        registerCells()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = 300
    }
    
    func registerCells() {
        guard let tableView = tableView else {
            return
        }
        tableView.register(
            UINib.init(nibName: RunningWorkoutGoalProgressCellTableViewCell.identifier, bundle: nil),
            forCellReuseIdentifier: RunningWorkoutGoalProgressCellTableViewCell.identifier)
        tableView.register(
        UINib.init(nibName: StepWorkoutGoalProgressTableViewCell.identifier, bundle: nil),
        forCellReuseIdentifier: StepWorkoutGoalProgressTableViewCell.identifier)
    }
    
    func makeCell(with viewModel: WorkoutGoalsProgressViewModel, indexPath: IndexPath) -> UITableViewCell? {
        let type = viewModel.sections[indexPath.section].workoutType
        
        switch type {
        case .running:
            return buildRunningWorkoutGoalProgressCell(with: viewModel.runningWorkoutGoalsProgress, indexPath: indexPath)
        case .walking:
            return buildWalkingWorkoutGoalProgressCell(with: viewModel.walkingWorkoutGoalsProgress, indexPath: indexPath)
        case .step:
            return buildStepWorkoutGoalProgressCell(with: viewModel.stepWorkoutGoalsProgress, indexPath: indexPath)
        default:
            return nil
        }
    }
    
    func buildRunningWorkoutGoalProgressCell(with viewModel: WorkoutGoalsProgressViewModel.WalkingRunningWorkoutGoalProgressViewModel?, indexPath: IndexPath) -> UITableViewCell {
        guard let runningWorkoutGoalProgressViewModel = viewModel else {
            return UITableViewCell(frame: .zero)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RunningWorkoutGoalProgressCellTableViewCell.identifier, for: indexPath) as! RunningWorkoutGoalProgressCellTableViewCell
        cell.selectionStyle = .none
        cell.config(with: runningWorkoutGoalProgressViewModel)
        
        return cell
    }
    
    func buildWalkingWorkoutGoalProgressCell(with viewModel: WorkoutGoalsProgressViewModel.WalkingRunningWorkoutGoalProgressViewModel?, indexPath: IndexPath) -> UITableViewCell {
        guard let walkingWorkoutGoalProgressViewModel = viewModel else {
            return UITableViewCell(frame: .zero)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RunningWorkoutGoalProgressCellTableViewCell.identifier, for: indexPath) as! RunningWorkoutGoalProgressCellTableViewCell
        cell.selectionStyle = .none
        cell.config(with: walkingWorkoutGoalProgressViewModel)
        
        return cell
    }
    
    func buildStepWorkoutGoalProgressCell(with viewModel: WorkoutGoalsProgressViewModel.StepWorkoutGoalProgressViewModel?, indexPath: IndexPath) -> UITableViewCell {
        guard let stepWorkoutGoalProgressViewModel = viewModel else {
            return UITableViewCell(frame: .zero)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: StepWorkoutGoalProgressTableViewCell.identifier, for: indexPath) as! StepWorkoutGoalProgressTableViewCell
        cell.selectionStyle = .none
        cell.config(with: stepWorkoutGoalProgressViewModel)
        
        return cell
    }
}

extension WorkoutGoalsProgressViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.sections.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let viewModel = viewModel,
            let cell = makeCell(with: viewModel, indexPath: indexPath)
        else {
            return UITableViewCell(frame: .zero)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel?.sections[section].workoutType.rawValue.capitalized
    }
}

//
//  WorkoutGoalsProgressPresenter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
import UIKit

protocol WorkoutGoalsProgressPresenterProtocol {
    func formatWorkoutGoalsProgress(with workoutGoalsProgress: [WorkoutGoalProgress], workoutGoals: [WorkoutGoal])
    func formatErrorAlert(message: String, action: (() -> Void)?)
    func formatDailyHeartRate(with heartRateData: [HeartRateData])
}

final class WorkoutGoalsProgressPresenter {
    struct Dependencies {
           
    }
       
    let dependencies: Dependencies
    weak var view: WorkoutGoalsProgressViewController?
    var runningWorkoutGoalsProgress: [RunningWorkoutGoalProgress] = []
    var walkingWorkoutGoalsProgress: [WalkingWorkoutGoalProgress] = []
    var stepWorkoutGoalsProgress: [StepWorkoutGoalProgress] = []
       
    init(depedencies: Dependencies = .init()) {
           self.dependencies = depedencies
    }
}

extension WorkoutGoalsProgressPresenter: WorkoutGoalsProgressPresenterProtocol {
    func formatDailyHeartRate(with heartRateData: [HeartRateData]) {
        var heartRate: Double = 0
        heartRateData.forEach { heartRate += $0.bpm }
        
        heartRate = heartRate > 0 ? heartRate / Double(heartRateData.count) : 0
        
        
        
        
        view?.showDailyHeartRateAverage(with: HeartRateVieModel(heartRate: heartRate,
                                                                heartRateimage: UIImage(named: "heartRate-icon")))
    }
    
    func formatErrorAlert(message: String, action: (() -> Void)?) {
        view?.displayErrorAlert(title: "Error", message: message, action: action)
    }
    
    func formatWorkoutGoalsProgress(with workoutGoalsProgress: [WorkoutGoalProgress], workoutGoals: [WorkoutGoal]) {
        resetWorkoutGoalsProgress()
        workoutGoalsProgress.forEach {
            $0.accept(visitor: self, additionalInfo: ())
        }
        
        let runningWorkoutGoalsProgressViewModel = buildRunningWorkoutGoalsProgressViewModel(with: workoutGoalsProgress,
                                                                                             runningWorkoutGoals: workoutGoals.filter { $0.workoutType == .running })
        let walkingWorkoutGoalsProgressViewModel = buildWalkingWorkoutGoalsProgressViewModel(with: workoutGoalsProgress,
                                                                                             runningWorkoutGoals: workoutGoals.filter { $0.workoutType == .walking })
        let stepWorkoutGoalsProgressViewModel = buildStepsWorkoutGoalsProgressViewModel(with: workoutGoalsProgress,
                                                                                        stepWorkoutGoals: workoutGoals.filter { $0.workoutType == .step })
        
        let viewModel: WorkoutGoalsProgressViewModel = .init(runningWorkoutGoalsProgress: runningWorkoutGoalsProgressViewModel,
                                                             walkingWorkoutGoalsProgress: walkingWorkoutGoalsProgressViewModel,
                                                             stepWorkoutGoalsProgress: stepWorkoutGoalsProgressViewModel,
                                                             sections: [WorkoutGoalsProgressViewModel.WorkoutGoalProgressSection(workoutType: .step),
                                                                        WorkoutGoalsProgressViewModel.WorkoutGoalProgressSection(workoutType: .running),
                                                                        WorkoutGoalsProgressViewModel.WorkoutGoalProgressSection(workoutType: .walking)
                                                                        ])
        
        view?.updateView(with: viewModel)
    }
}

extension WorkoutGoalsProgressPresenter: WorkoutGoalProgressVisitor {
    func visit(runningWorkoutGoalProgress: RunningWorkoutGoalProgress, additionalInfo: ()) {
        runningWorkoutGoalsProgress.append(runningWorkoutGoalProgress)
    }
    
    func visit(stepWorkoutGoalProgress: StepWorkoutGoalProgress, additionalInfo: ()) {
        stepWorkoutGoalsProgress.append(stepWorkoutGoalProgress)
    }
    
    func visit(walkingWorkoutGoalProgress: WalkingWorkoutGoalProgress, additionalInfo: ()) {
        walkingWorkoutGoalsProgress.append(walkingWorkoutGoalProgress)
    }
}

private extension WorkoutGoalsProgressPresenter {
    struct Constants {
        static let noMedal = "greyMedal-icon"
        static let bronzeMedal = "bronze-icon"
        static let silverMedal = "silver-icon"
        static let goldMedal = "gold-icon"
        static let zombieMedal = "zombie-icon"
        static let noGoalsAchived = "Keep it going"
        static let noNextGoals = "You've completed the daily goals"
    }
    func buildRunningWorkoutGoalsProgressViewModel(with workoutGoalsProgress: [WorkoutGoalProgress], runningWorkoutGoals: [WorkoutGoal]) -> WorkoutGoalsProgressViewModel.WalkingRunningWorkoutGoalProgressViewModel {
        var totalDistance: Double = 0
        var totalActiveEnergy: Double = 0

        runningWorkoutGoalsProgress.forEach {
            totalActiveEnergy += $0.activeEnergy ?? 0
            totalDistance += $0.distance ?? 0
        }

        let totalDistanceKM = totalDistance > 0 ? totalDistance / 1000 : 0

        let currentWorkoutGoalsAchived = runningWorkoutGoals.filter { totalDistance >= $0.goal }.sorted(by: { $0.goal < $1.goal })

        let currentPoints = calculatePointsEarned(workoutGoals: currentWorkoutGoalsAchived)
        let currentMedalImage = getCurrentMedalImage(with: currentWorkoutGoalsAchived.last)
        let currentWorkoutAchived = currentWorkoutGoalsAchived.last?.title ?? Constants.noGoalsAchived

        let nextWorkoutGoal = runningWorkoutGoals.filter { totalDistance < $0.goal }.sorted(by: { $0.goal < $1.goal }).first

        let titleNextWorkoutGoal = getNextGoal(workoutGoal: nextWorkoutGoal)
        let pointsToEarn = nextWorkoutGoal?.reward.points ?? 0

        return WorkoutGoalsProgressViewModel.WalkingRunningWorkoutGoalProgressViewModel(currentDistance: totalDistanceKM,
                                                                                        currentActiveEnergy: totalActiveEnergy,
                                                                                        currentPoints: currentPoints,
                                                                                        currentMedal: currentMedalImage,
                                                                                        currentWorkoutAchived: currentWorkoutAchived,
                                                                                        nextWorkoutGoal: titleNextWorkoutGoal,
                                                                                        pointsToEarn: pointsToEarn)
    }
    
    func buildWalkingWorkoutGoalsProgressViewModel(with workoutGoalsProgress: [WorkoutGoalProgress], runningWorkoutGoals: [WorkoutGoal]) -> WorkoutGoalsProgressViewModel.WalkingRunningWorkoutGoalProgressViewModel {
        var totalDistance: Double = 0
        var totalActiveEnergy: Double = 0
        
        walkingWorkoutGoalsProgress.forEach {
            totalActiveEnergy += $0.activeEnergy ?? 0
            totalDistance += $0.distance ?? 0
        }
        
        let totalDistanceKM = totalDistance > 0 ? totalDistance / 1000 : 0
        
        let currentWorkoutGoalsAchived = runningWorkoutGoals.filter { totalDistance >= $0.goal }.sorted(by: { $0.goal < $1.goal })
        
        let currentPoints = calculatePointsEarned(workoutGoals: currentWorkoutGoalsAchived)
        let currentMedalImage = getCurrentMedalImage(with: currentWorkoutGoalsAchived.last)
        let currentWorkoutAchived = currentWorkoutGoalsAchived.last?.title ?? Constants.noGoalsAchived
        
        let nextWorkoutGoal = runningWorkoutGoals.filter { totalDistance < $0.goal }.sorted(by: { $0.goal < $1.goal }).first
        
        let titleNextWorkoutGoal = getNextGoal(workoutGoal: nextWorkoutGoal)
        let pointsToEarn = nextWorkoutGoal?.reward.points ?? 0
        
        return WorkoutGoalsProgressViewModel.WalkingRunningWorkoutGoalProgressViewModel(currentDistance: totalDistanceKM,
                                                                                        currentActiveEnergy: totalActiveEnergy,
                                                                                        currentPoints: currentPoints,
                                                                                        currentMedal: currentMedalImage,
                                                                                        currentWorkoutAchived: currentWorkoutAchived,
                                                                                        nextWorkoutGoal: titleNextWorkoutGoal,
                                                                                        pointsToEarn: pointsToEarn)
    }
    
    func buildStepsWorkoutGoalsProgressViewModel(with workoutGoalsProgress: [WorkoutGoalProgress], stepWorkoutGoals: [WorkoutGoal]) -> WorkoutGoalsProgressViewModel.StepWorkoutGoalProgressViewModel {
        var steps: Double = 0
        
        stepWorkoutGoalsProgress.forEach {
            steps += Double($0.steps ?? 0)
        }
        
        let currentWorkoutGoalsAchived = stepWorkoutGoals.filter { steps >= $0.goal }.sorted(by: { $0.goal < $1.goal })
        
        let currentPoints = calculatePointsEarned(workoutGoals: currentWorkoutGoalsAchived)
        let currentMedalImage = getCurrentMedalImage(with: currentWorkoutGoalsAchived.last)
        let currentWorkoutAchived = currentWorkoutGoalsAchived.last?.title ?? Constants.noGoalsAchived
        
        let nextWorkoutGoal = stepWorkoutGoals.filter { steps < $0.goal }.sorted(by: { $0.goal < $1.goal }).first
        
        let titleNextWorkoutGoal = getNextGoal(workoutGoal: nextWorkoutGoal)
        let pointsToEarn = nextWorkoutGoal?.reward.points ?? 0
        
        return WorkoutGoalsProgressViewModel.StepWorkoutGoalProgressViewModel(currentSteps: steps,
                                                                              currentPoints: currentPoints,
                                                                              currentMedal: currentMedalImage,
                                                                              currentWorkoutAchived: currentWorkoutAchived,
                                                                              nextWorkoutGoal: titleNextWorkoutGoal,
                                                                              pointsToEarn: pointsToEarn)
    }
    
    func calculatePointsEarned(workoutGoals: [WorkoutGoal]) -> Int {
        var pointsEarned = 0
        
        workoutGoals.forEach {
            pointsEarned += $0.reward.points
        }
        
        return pointsEarned
    }
    
    func getCurrentMedalImage(with workoutGoal: WorkoutGoal?) -> UIImage? {
        guard let workoutGoal = workoutGoal else {
            return UIImage(named: Constants.noMedal)
        }
        
        switch workoutGoal.medal {
        case .bronze:
            return UIImage(named: Constants.bronzeMedal)
        case .silver:
            return UIImage(named: Constants.silverMedal)
        case .gold:
            return UIImage(named: Constants.goldMedal)
        case .zombie:
            return UIImage(named: Constants.zombieMedal)
        case .none:
            return UIImage(named: Constants.noMedal)
        }
    }
    
    func getNextGoal(workoutGoal: WorkoutGoal?) -> String {
        guard
            let tite = workoutGoal?.title,
            let goalText = workoutGoal?.goalText
        else {
            return Constants.noNextGoals
        }
        
        return String(format: "%@ - %@", tite, goalText)
    }
    
    func resetWorkoutGoalsProgress() {
        runningWorkoutGoalsProgress = []
        walkingWorkoutGoalsProgress = []
        stepWorkoutGoalsProgress = []
    }
}

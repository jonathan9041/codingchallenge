//
//  WorkoutGoalsProgressTableViewCell.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit

class WorkoutGoalsProgressTableViewCell: UITableViewCell {
    static let identifier: String = "WorkoutGoalsProgressTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
//        workoutGoalTypeImage.image = nil
//        workoutGoalTrophyImage.image = nil
//        title.text = nil
//        subtitle.text = nil
//        points.text = nil
//        goal.text = nil
    }
    
//    func config(with viewModel: WorkoutGoalsProgressViewModel.WorkoutGoalProgressViewModel) {
//        workoutGoalTypeImage.image = viewModel.workoutGoalTypeImage
//        workoutGoalTrophyImage.image = viewModel.workoutGoalTrophyImage
//        title.text = viewModel.title
//        subtitle.text = viewModel.subtitle
//        points.text = String(format: "%d Points", viewModel.points)
//        goal.text = String(format: "%d", viewModel.goal)
//    }
}

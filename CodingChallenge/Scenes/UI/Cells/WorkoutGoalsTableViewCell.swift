//
//  WorkoutGoalsTableViewCell.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/21/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit

class WorkoutGoalsTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var points: UILabel!
    @IBOutlet weak var goal: UILabel!
    @IBOutlet weak var workoutGoalTypeImage: UIImageView!
    @IBOutlet weak var workoutGoalTrophyImage: UIImageView!
    
    static let identifier: String = "WorkoutGoalsTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        workoutGoalTypeImage.image = nil
        workoutGoalTrophyImage.image = nil
        title.text = nil
        subtitle.text = nil
        points.text = nil
        goal.text = nil
    }
    
    func config(with viewModel: WorkoutGoalsViewModel.WorkoutGoalViewModel) {
        workoutGoalTypeImage.image = viewModel.workoutGoalTypeImage
        workoutGoalTrophyImage.image = viewModel.workoutGoalTrophyImage
        title.text = viewModel.title
        subtitle.text = viewModel.subtitle
        points.text = String(format: "%d Points", viewModel.points)
        goal.text = viewModel.goalText
    }
}

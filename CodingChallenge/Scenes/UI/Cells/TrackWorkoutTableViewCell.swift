//
//  TrackWorkoutTableViewCell.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit



class TrackWorkoutTableViewCell: UITableViewCell {
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var labelTextField: UILabel!
    
    static let identifier: String = "TrackWorkoutTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        valueTextField.text = nil
        labelTextField.text = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func config(label: String) {
        labelTextField.text = label
    }
    
    func getTextFieldValue() -> String {
        return valueTextField.text ?? ""
    }
    
    func resetFieldValue() {
        valueTextField.text = nil
    }
}

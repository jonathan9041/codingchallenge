//
//  SaveWorkoutHeaderFooterView.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/23/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit

protocol SaveWorkoutHeaderFooterViewDelegate: class {
    func didTapSaveWorkout()
}

class SaveWorkoutHeaderFooterView: UITableViewHeaderFooterView {
    static let identifier: String = "SaveWorkoutHeaderFooterView"
    
    weak var delegate: SaveWorkoutHeaderFooterViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func didTapSaveWorkout(_ sender: Any) {
        delegate?.didTapSaveWorkout()
    }
}

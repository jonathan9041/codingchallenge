//
//  TrackingWorkoutMenuTableViewCell.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/23/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit

class TrackingWorkoutMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var workoutType: UILabel!
    
    static let identifier: String = "TrackingWorkoutMenuTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        workoutType.text = nil
    }
    
    func config(with type: String) {
        workoutType.text = type
    }
}

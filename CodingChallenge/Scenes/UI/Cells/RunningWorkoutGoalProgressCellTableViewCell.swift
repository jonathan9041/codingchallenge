//
//  RunningWorkoutGoalProgressCellTableViewCell.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/23/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit

class RunningWorkoutGoalProgressCellTableViewCell: UITableViewCell {
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var activeEnergyLabel: UILabel!
    @IBOutlet weak var goalAchivedLabel: UILabel!
    @IBOutlet weak var pointsEarnedLabel: UILabel!
    @IBOutlet weak var nextGoalLabel: UILabel!
    @IBOutlet weak var nextPointsToEarnLabel: UILabel!
    @IBOutlet weak var currentMedalImage: UIImageView!
    
    static let identifier: String = "RunningWorkoutGoalProgressCellTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        distanceLabel.text = nil
        activeEnergyLabel.text = nil
        goalAchivedLabel.text = nil
        pointsEarnedLabel.text = nil
        nextGoalLabel.text = nil
        nextPointsToEarnLabel.text = nil
        currentMedalImage.image = nil
    }
    
    func config(with viewModel: WorkoutGoalsProgressViewModel.WalkingRunningWorkoutGoalProgressViewModel) {
        distanceLabel.text = String(format: "%.1f km", viewModel.currentDistance)
        activeEnergyLabel.text = String(format: "%.1f kcal", viewModel.currentActiveEnergy)
        goalAchivedLabel.text = viewModel.currentWorkoutAchived
        pointsEarnedLabel.text = String(format: "%d points earned", viewModel.currentPoints)
        nextGoalLabel.text = String(format: "Next goal: %@", viewModel.nextWorkoutGoal)
        nextPointsToEarnLabel.text = String(format: "- Earn %d points", viewModel.pointsToEarn)
        currentMedalImage.image = viewModel.currentMedal
        nextPointsToEarnLabel.text = viewModel.pointsToEarn > 0 ? String(format: "- Earn %d points", viewModel.pointsToEarn) : ""
    }
}

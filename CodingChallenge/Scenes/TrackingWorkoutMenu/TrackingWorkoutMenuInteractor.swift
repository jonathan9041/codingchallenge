//
//  TrackingWorkoutMenuInteractor.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/23/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol TrackingWorkoutMenuInteractorProtocol {
    func handleSceneDidLoad()
    func handleSceneWillAppear()
    func didSelectCell(with type: WorkoutType)
}

final class TrackingWorkoutMenuInteractor: TrackingWorkoutMenuInteractorProtocol {
    struct Dependencies {
        let requestHealthInformationAccess: RequestHealthInformationAccess = RequestHealthInformationAccessAdapter()
    }
    
    let dependencies: Dependencies
    var presenter: TrackingWorkoutMenuPresenterProtocol?
    
    init(depedencies: Dependencies = .init()) {
        self.dependencies = depedencies
    }
    
    func handleSceneDidLoad() {
        presenter?.formatMenu()
    }
    
    func handleSceneWillAppear() {
        
    }
    
    func didSelectCell(with type: WorkoutType) {
        presenter?.formatCellSelection(with: type)
    }
}

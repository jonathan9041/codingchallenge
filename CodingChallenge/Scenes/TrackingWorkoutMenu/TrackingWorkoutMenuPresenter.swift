//
//  TrackingWorkoutMenuPresenter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/23/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol TrackingWorkoutMenuPresenterProtocol {
    func formatMenu()
    func formatCellSelection(with type: WorkoutType)
}

final class TrackingWorkoutMenuPresenter {
    struct Dependencies {
            
     }
        
    let dependencies: Dependencies
    weak var view: TrackingWorkoutMenuViewController?
        
    init(depedencies: Dependencies = .init()) {
            self.dependencies = depedencies
    }
}

extension TrackingWorkoutMenuPresenter: TrackingWorkoutMenuPresenterProtocol {
    func formatCellSelection(with type: WorkoutType) {
        view?.goToTrackWorkoutScreen(with: type)
    }
    
    func formatMenu() {
        let viewModel = TrackWorkoutMenuViewModel(workouts: [.running, .step, .walking])
        view?.updateMenu(with: viewModel)
    }
}

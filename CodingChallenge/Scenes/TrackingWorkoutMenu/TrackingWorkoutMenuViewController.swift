//
//  TrackingWorkoutMenuViewController.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/23/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit

protocol TrackingWorkoutMenuViewControllerProtocol {
    func goToTrackWorkoutScreen(with type: WorkoutType)
    func updateMenu(with viewModel: TrackWorkoutMenuViewModel)
}

class TrackingWorkoutMenuViewController: UIViewController {
    struct Constants {
        static let trackWorkoutSegue = "trackWorkoutSegue"
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var interactor: TrackingWorkoutMenuInteractorProtocol?
    var viewModel: TrackWorkoutMenuViewModel?
    var workTypeSelected: WorkoutType = .unknown
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        interactor?.handleSceneDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.handleSceneWillAppear()
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            segue.identifier == Constants.trackWorkoutSegue,
            let destinationController = segue.destination as? TrackingWorkoutViewController
        else {
            return
        }
        
        destinationController.workoutType = workTypeSelected
    }
}

extension TrackingWorkoutMenuViewController: TrackingWorkoutMenuViewControllerProtocol {
    func goToTrackWorkoutScreen(with type: WorkoutType) {
        workTypeSelected = type
        performSegue(withIdentifier: Constants.trackWorkoutSegue, sender: self)
    }
    
    func updateMenu(with viewModel: TrackWorkoutMenuViewModel) {
        self.viewModel = viewModel
        
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}


private extension TrackingWorkoutMenuViewController {
    func setup() {
        let interactor = TrackingWorkoutMenuInteractor()
        let presenter = TrackingWorkoutMenuPresenter()
        presenter.view = self
        interactor.presenter = presenter
        self.interactor = interactor
        configTableView()
    }
    
    func configTableView() {
        registerCells()
        self.title = "Tracking"
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = 300
    }
    
    func registerCells() {
        guard let tableView = tableView else {
            return
        }
        tableView.register(
            UINib.init(nibName: TrackingWorkoutMenuTableViewCell.identifier, bundle: nil),
            forCellReuseIdentifier: TrackingWorkoutMenuTableViewCell.identifier)
    }
}

extension TrackingWorkoutMenuViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.workouts.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else {
            return UITableViewCell(frame: .zero)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TrackingWorkoutMenuTableViewCell.identifier, for: indexPath) as! TrackingWorkoutMenuTableViewCell
        cell.selectionStyle = .none
        
        cell.config(with: viewModel.workouts[indexPath.row].rawValue.capitalized)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else {
            return
        }
        
        interactor?.didSelectCell(with: viewModel.workouts[indexPath.row])
    }
}

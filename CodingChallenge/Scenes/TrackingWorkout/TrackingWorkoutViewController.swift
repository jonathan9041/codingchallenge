//
//  TrackingWorkoutViewController.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit

protocol TrackingWorkoutViewControllerProtocol {
    func updateView(with viewModel: TrackWorkoutViewModel)
    func displayAlert(title: String, message: String, action: (() -> Void)?)
}

final class TrackingWorkoutViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var interactor: TrackingWorkoutInteractorProtocol?
    var workoutType: WorkoutType = .unknown
    var viewModel: TrackWorkoutViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        interactor?.handleSceneDidLoad()
        interactor?.didSetWorkoutType(workoutType)
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.handleSceneWillAppear()
    }
}

extension TrackingWorkoutViewController: TrackingWorkoutViewControllerProtocol {
    func displayAlert(title: String, message: String, action: (() -> Void)?) {
        displayAlert(title: title, message: message, acceptButtonText: "Accept", completion: action)
    }
    
    func updateView(with viewModel: TrackWorkoutViewModel) {
        self.viewModel = viewModel
        
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

private extension TrackingWorkoutViewController {
    func setup() {
        self.title = workoutType.rawValue.capitalized
        let interactor = TrackingWorkoutInteractor()
        let presenter = TrackingWorkoutPresenter()
        presenter.view = self
        interactor.presenter = presenter
        self.interactor = interactor
        configTableView()
    }
    
    func configTableView() {
        registerCells()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .singleLine
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = 100
    }
    
    func registerCells() {
        guard let tableView = tableView else {
            return
        }
        tableView.register(
            UINib.init(nibName: TrackWorkoutTableViewCell.identifier, bundle: nil),
            forCellReuseIdentifier: TrackWorkoutTableViewCell.identifier)
        tableView.register(UINib.init(nibName: SaveWorkoutHeaderFooterView.identifier, bundle: nil),
                           forHeaderFooterViewReuseIdentifier: SaveWorkoutHeaderFooterView.identifier)
    }
    
    func createFieldValues() -> [String: String]? {
        guard let viewModel = viewModel else {
            return nil
        }
        
        var fields: [String: String] = [:]
        
        viewModel.fields.enumerated().forEach { (index, field) in
            guard let cellValue = getCellValue(index: index) else {
                return
            }
            
            fields[field.key] = cellValue
        }
        
        return fields
    }
    
    func getCellValue(index: Int) -> String? {
        let indexPath = IndexPath(row: index, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) as? TrackWorkoutTableViewCell else {
            return nil
        }
        
        let value = cell.getTextFieldValue()
        cell.resetFieldValue()
        
        return value
    }
}

extension TrackingWorkoutViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.fields.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else {
            return UITableViewCell(frame: .zero)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TrackWorkoutTableViewCell.identifier, for: indexPath) as! TrackWorkoutTableViewCell
        cell.selectionStyle = .none
        
        cell.config(label: viewModel.fields[indexPath.row].label)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: SaveWorkoutHeaderFooterView.identifier) as! SaveWorkoutHeaderFooterView
        cell.delegate = self
        return cell
    }
}

extension TrackingWorkoutViewController: SaveWorkoutHeaderFooterViewDelegate {
    func didTapSaveWorkout() {
        guard let fields = createFieldValues() else {
            return
        }
        
        interactor?.handleFieldsValues(with: workoutType, values: fields)
    }
}

//
//  TrackingWorkoutInteractor.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol TrackingWorkoutInteractorProtocol {
    func handleSceneDidLoad()
    func handleSceneWillAppear()
    func didSetWorkoutType(_ type: WorkoutType)
    func handleFieldsValues(with type: WorkoutType, values: [String: String])
}

final class TrackingWorkoutInteractor {
    struct Dependencies {
        let trackWalkingRunningWorkout: TrackWalkingRunningWorkout = TrackWalkingRunningWorkoutAdapter()
        let trackStepsWorkout: TrackStepsWorkout = TrackStepsWorkoutAdapter()
        let requestHealthInformationAccess: RequestHealthInformationAccess = RequestHealthInformationAccessAdapter()
    }
    
    let dependencies: Dependencies
    var presenter: TrackingWorkoutPresenterProtocol?
    var authorized: Bool = false
    
    init(depedencies: Dependencies = .init()) {
        self.dependencies = depedencies
    }
    
    
}

extension TrackingWorkoutInteractor: TrackingWorkoutInteractorProtocol {
    func handleSceneWillAppear() {
    }
       
    func handleSceneDidLoad() {
        requestHealthInformationAccess()
    }
    
    func didSetWorkoutType(_ type: WorkoutType) {
        guard type != .unknown else {
            return
        }
        
        presenter?.formatWorkoutFields(with: type)
    }
    
    func handleFieldsValues(with type: WorkoutType, values: [String: String]) {
        guard authorized else {
            return
        }
        
        switch type {
        case .running, .walking:
            guard
                let distanceValue = values["distance"],
                let durationValue = values["duration"],
                let distance = Double(distanceValue),
                let duration = TimeInterval(durationValue)
            else {
                return
            }
            
            trackRunningWalkingWorkout(with: type, distance: distance, duration: duration)
        case .step:
            guard
                let stepsValue = values["steps"],
                let durationValue = values["duration"],
                let steps = Int(stepsValue),
                let duration = TimeInterval(durationValue)
            else {
                return
            }
            
            trackStepsWorkout(steps: steps, duration: duration)
        case .unknown:
            break
        }
    }
}

private extension TrackingWorkoutInteractor {
    func trackRunningWalkingWorkout(with type: WorkoutType, distance: Double, duration: TimeInterval) {
        dependencies.trackWalkingRunningWorkout.execute(with: type, distance: distance, duration: duration) { [weak self] (tracked, error) in
            guard error == nil else {
                self?.presenter?.formatErrorAlert(message: error?.localizedDescription ?? "Getting error to track running workout", action: nil)
                return
            }
            
            guard tracked else {
                self?.presenter?.formatErrorAlert(message: "The workout was not tracked", action: nil)
                return
            }
            
            self?.presenter?.formatAlert(message: String(format: "%@ workout has been tracked!", type.rawValue.capitalized), action: nil)
        }
    }
    
    func trackStepsWorkout(steps: Int, duration: TimeInterval) {
        dependencies.trackStepsWorkout.execute(steps: steps, duration: duration) { [weak self] (tracked, error) in
            guard error == nil else {
                self?.presenter?.formatErrorAlert(message: error?.localizedDescription ?? "Getting error to track step workout", action: nil)
                return
            }
            
            guard tracked else {
                self?.presenter?.formatErrorAlert(message: "The workout was not tracked", action: nil)
                return
            }
            
            self?.presenter?.formatAlert(message: "Step workout has been tracked!", action: nil)
        }
    }
    
    func requestHealthInformationAccess() {
        dependencies.requestHealthInformationAccess.execute { [weak self] (authorized, error) in
            guard error == nil else {
                self?.presenter?.formatErrorAlert(message: "Error with HealthInformationAccess. \(error?.localizedDescription ?? "")", action: nil)
                return
            }
            
            guard authorized else {
                self?.presenter?.formatErrorAlert(message: "Health information Access Error. Unauthorized access", action: nil)
                return
            }
            
            self?.authorized = true
        }
    }
}

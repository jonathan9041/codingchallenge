//
//  TrackingWorkoutPresenter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
import UIKit

protocol TrackingWorkoutPresenterProtocol {
    func formatWorkoutFields(with type: WorkoutType)
    func formatAlert(message: String, action: (() -> Void)?)
    func formatErrorAlert(message: String, action: (() -> Void)?)
}

final class TrackingWorkoutPresenter {
    struct Dependencies {
           
    }
       
   let dependencies: Dependencies
   weak var view: TrackingWorkoutViewController?
       
    init(depedencies: Dependencies = .init()) {
           self.dependencies = depedencies
    }
}

extension TrackingWorkoutPresenter: TrackingWorkoutPresenterProtocol {
    func formatErrorAlert(message: String, action: (() -> Void)?) {
        view?.displayAlert(title: "error", message: message, action: action)
    }
    func formatAlert(message: String, action: (() -> Void)?) {
        view?.displayAlert(title: "Tracking Workout", message: message, action: action)
    }
    
    func formatWorkoutFields(with type: WorkoutType) {
        var viewModel: TrackWorkoutViewModel?
        switch type {
        case .running:
            viewModel = buildRunningFieldsViewModel()
        case .walking:
            viewModel = buildWalkingFieldsViewModel()
        case .step:
            viewModel = buildStepFieldsViewModel()
        case .unknown:
            break
        }
        
        guard let trackViewModel = viewModel else {
            return
        }
        
        view?.updateView(with: trackViewModel)
    }
}

private extension TrackingWorkoutPresenter {
    func buildRunningFieldsViewModel() -> TrackWorkoutViewModel {
        return TrackWorkoutViewModel(fields: [.init(label: "Distance: (meters)", key: "distance"),
                                              .init(label: "Duration: (min)", key: "duration")])
    }
    
    func buildWalkingFieldsViewModel() -> TrackWorkoutViewModel {
        return TrackWorkoutViewModel(fields: [.init(label: "Distance: (meters)", key: "distance"),
                                              .init(label: "Duration: (min)", key: "duration")])
    }
    
    func buildStepFieldsViewModel() -> TrackWorkoutViewModel {
        return TrackWorkoutViewModel(fields: [.init(label: "Steps:", key: "steps"),
                                              .init(label: "Duration: (min)", key: "duration")])
    }
}

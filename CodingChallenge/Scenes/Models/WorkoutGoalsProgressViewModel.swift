//
//  WorkoutGoalsProgressViewModel.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
import UIKit

struct WorkoutGoalsProgressViewModel {
    struct WalkingRunningWorkoutGoalProgressViewModel {
        var currentDistance: Double
        var currentActiveEnergy: Double
        var currentPoints: Int
        var currentMedal: UIImage?
        var currentWorkoutAchived: String
        var nextWorkoutGoal: String
        var pointsToEarn: Int
    }
    
    struct StepWorkoutGoalProgressViewModel {
        var currentSteps: Double
        var currentPoints: Int
        var currentMedal: UIImage?
        var currentWorkoutAchived: String
        var nextWorkoutGoal: String
        var pointsToEarn: Int
    }
    
    struct WorkoutGoalProgressSection {
        var workoutType: WorkoutType
    }
    
    var runningWorkoutGoalsProgress: WalkingRunningWorkoutGoalProgressViewModel?
    var walkingWorkoutGoalsProgress: WalkingRunningWorkoutGoalProgressViewModel?
    var stepWorkoutGoalsProgress: StepWorkoutGoalProgressViewModel?
    var sections: [WorkoutGoalProgressSection]
}

struct HeartRateVieModel {
    var heartRate: Double
    var heartRateimage: UIImage?
}

extension HeartRateVieModel {
    var heartRateText: String {
        let formatString = heartRate.truncatingRemainder(dividingBy: 1) == 0 ? "Heart rate average %.0f BPM" : "Heart rate average %.1f BPM"
        
        return String(format: formatString, heartRate)
    }
}

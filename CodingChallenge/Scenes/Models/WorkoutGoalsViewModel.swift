//
//  WorkoutGoalsViewModel.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
import UIKit

struct WorkoutGoalsViewModel {
    struct WorkoutGoalViewModel {
        var workoutGoalTypeImage: UIImage?
        var workoutGoalTrophyImage: UIImage?
        var title: String
        var subtitle: String
        var points: Int
        var goal: Double
        var goalText: String
        var workoutType: WorkoutType
    }
    
    struct Section {
        var workoutType: WorkoutType
        var workoutGoals: [WorkoutGoalViewModel]
    }
    
    var workoutGoals: [Section]
}

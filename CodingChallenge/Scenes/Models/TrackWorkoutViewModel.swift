//
//  TrackWorkoutViewModel.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/23/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

struct TrackWorkoutViewModel {
    struct WorkoutField {
        var label: String
        var key: String
    }
    
    var fields: [WorkoutField]
}

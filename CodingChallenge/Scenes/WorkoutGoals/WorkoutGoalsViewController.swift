//
//  WorkoutGoalsViewController.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/21/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import UIKit

protocol WorkoutGoalsViewControllerProtocol {
    func updateView(with viewModel: WorkoutGoalsViewModel)
    func displayErrorAlert(title: String, message: String, action: (() -> Void)?)
}

final class WorkoutGoalsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var interactor: WorkoutGoalsInteractorProtocol?
    var viewModel: WorkoutGoalsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        interactor?.handleSceneDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.handleSceneWillAppear()
    }
}

extension WorkoutGoalsViewController: WorkoutGoalsViewControllerProtocol {
    func displayErrorAlert(title: String, message: String, action: (() -> Void)?) {
        displayAlert(title: title, message: message, acceptButtonText: "Accept", completion: action)
    }
    
    func updateView(with viewModel: WorkoutGoalsViewModel) {
        self.viewModel = viewModel
        
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

private extension WorkoutGoalsViewController{
    func setup() {
        let interactor = WorkoutGoalsInteractor()
        let presenter = WorkoutGoalsPresenter()
        presenter.view = self
        interactor.presenter = presenter
        self.interactor = interactor
        configTableView()
    }
    
    func configTableView() {
        registerCells()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .singleLine
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = 300
    }
    
    func registerCells() {
        guard let tableView = tableView else {
            return
        }
        tableView.register(
            UINib.init(nibName: WorkoutGoalsTableViewCell.identifier, bundle: nil),
            forCellReuseIdentifier: WorkoutGoalsTableViewCell.identifier)
    }
}

extension WorkoutGoalsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.workoutGoals.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel?.workoutGoals[section].workoutType.rawValue.capitalized
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.workoutGoals[section].workoutGoals.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let workoutGoalViewModel = viewModel?.workoutGoals[indexPath.section].workoutGoals[indexPath.row] else {
            return UITableViewCell(frame: .zero)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: WorkoutGoalsTableViewCell.identifier, for: indexPath) as! WorkoutGoalsTableViewCell
        cell.selectionStyle = .none
        cell.config(with: workoutGoalViewModel)
        
        return cell
    }
}

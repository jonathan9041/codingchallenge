//
//  WorkoutGoalsInteractor.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/21/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol WorkoutGoalsInteractorProtocol {
    func handleSceneDidLoad()
    func handleSceneWillAppear()
}

final class WorkoutGoalsInteractor: WorkoutGoalsInteractorProtocol{
    struct Dependencies {
        let getWorkoutGoals: GetWorkoutGoals = GetWorkoutGoalsAdapter()
        let requestHealthInformationAccess: RequestHealthInformationAccess = RequestHealthInformationAccessAdapter()
    }
    
    let dependencies: Dependencies
    var presenter: WorkoutGoalsPresenterProtocol?
    var authorized: Bool = false
    
    init(depedencies: Dependencies = .init()) {
        self.dependencies = depedencies
    }
    
    func handleSceneWillAppear() {
        guard authorized else {
            return
        }
        
        refreshWorkoutGoals()
    }
       
    func handleSceneDidLoad() {
        requestHealthInformationAccess()
    }
}

private extension WorkoutGoalsInteractor {
    func refreshWorkoutGoals() {
        dependencies.getWorkoutGoals.execute { [weak self] (response) in
            switch response {
            case .success(let workoutGoals):
                self?.presenter?.formatWorkoutGoals(with: workoutGoals)
            case .failure(let error):
                self?.presenter?.formatErrorAlert(message: error.localizedDescription, action: nil)
            }
        }
    }
    
    func requestHealthInformationAccess() {
        dependencies.requestHealthInformationAccess.execute { [weak self] (authorized, error) in
            guard error == nil else {
                self?.presenter?.formatErrorAlert(message: error?.localizedDescription ?? "Unauthorized to retrive health information", action: nil)
                return
            }
            
            guard authorized else {
                self?.presenter?.formatErrorAlert(message: "Health information Access Error. Unauthorized access", action: nil)
                return
            }
            
            self?.authorized = true
            self?.refreshWorkoutGoals()
        }
    }
}

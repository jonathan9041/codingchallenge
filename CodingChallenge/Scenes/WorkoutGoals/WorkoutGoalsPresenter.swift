//
//  WorkoutGoalsPresenter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/21/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
import UIKit

protocol WorkoutGoalsPresenterProtocol {
    func formatWorkoutGoals(with workoutGoals: [WorkoutGoal])
    func formatErrorAlert(message: String, action: (() -> Void)?)
}

final class WorkoutGoalsPresenter {
    struct Dependencies { }
       
   let dependencies: Dependencies
   weak var view: WorkoutGoalsViewController?
       
    init(depedencies: Dependencies = .init()) {
           self.dependencies = depedencies
    }
}

extension WorkoutGoalsPresenter: WorkoutGoalsPresenterProtocol {
    func formatErrorAlert(message: String, action: (() -> Void)?) {
        view?.displayErrorAlert(title: "Error", message: message, action: action)
    }
    
    func formatWorkoutGoals(with workoutGoals: [WorkoutGoal]) {
        let workoutGoalsViewModel = workoutGoals.map { buildWorkoutGoalViewModel(workoutGoal: $0) }
        let viewModelSections = createViewModelSections(with: workoutGoalsViewModel)
        let viewModel: WorkoutGoalsViewModel = .init(workoutGoals: viewModelSections)

        view?.updateView(with: viewModel)
    }
}

private extension WorkoutGoalsPresenter {
    func createViewModelSections(with workoutGoalsViewModel: [WorkoutGoalsViewModel.WorkoutGoalViewModel]) -> [WorkoutGoalsViewModel.Section] {
        let sections: [WorkoutGoalsViewModel.Section] = []
        let viewModelsByWorkoutType = workoutGoalsViewModel.reduce(into: sections) { previous, current in
            let workoutType = current.workoutType
            guard let index = previous.firstIndex(where: { $0.workoutType == workoutType }) else {
                let newSection = WorkoutGoalsViewModel.Section(workoutType: workoutType,
                                                               workoutGoals: [current])
                previous.append(newSection)
                return
            }
            
            let existing = previous[index].workoutGoals
            let final = existing + [current]
            
            previous[index].workoutGoals = final.sorted(by: { $0.goal < $1.goal })
        }
        
        return viewModelsByWorkoutType.sorted(by: { $0.workoutType.rawValue < $1.workoutType.rawValue })
    }
    
    func buildWorkoutGoalViewModel(workoutGoal: WorkoutGoal) -> WorkoutGoalsViewModel.WorkoutGoalViewModel {
        let workoutGoalViewModel = WorkoutGoalsViewModel.WorkoutGoalViewModel(workoutGoalTypeImage: getWorkoutTypeImage(with: workoutGoal.workoutType),
                                                                              workoutGoalTrophyImage: getWorkoutGoalMedalImage(with: workoutGoal.medal),
                                                                              title: workoutGoal.title,
                                                                              subtitle: workoutGoal.description,
                                                                              points: workoutGoal.reward.points,
                                                                              goal: workoutGoal.goal,
                                                                              goalText: workoutGoal.goalText,
                                                                              workoutType: workoutGoal.workoutType)
        return workoutGoalViewModel
    }
    
    func getWorkoutTypeImage(with type: WorkoutType) -> UIImage? {
        switch type {
        case .running:
            return UIImage(named: "running-icon")
        case .walking:
            return UIImage(named: "walking-icon")
        case .step:
            return UIImage(named: "steps-icon")
        case .unknown:
            return UIImage(named: "empty-icon")
        }
    }
    
    func getWorkoutGoalMedalImage(with medal: Medal) -> UIImage? {
        switch medal {
        case .bronze:
            return UIImage(named: "bronze-icon")
        case .silver:
            return UIImage(named: "silver-icon")
        case .gold:
            return UIImage(named: "gold-icon")
        case .zombie:
            return UIImage(named: "zombie-icon")
        case .none:
            return UIImage(named: "empty-icon")
        }
    }
}

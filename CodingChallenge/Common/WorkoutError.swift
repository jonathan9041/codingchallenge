//
//  WorkoutError.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/25/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

public enum WorkoutError: Error {
    case invalidInformation
    case emptyResult
    case serviceError
    case unknownServiceError
    case unknownError
    case customError(description: String)
}

extension WorkoutError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidInformation:
            return NSLocalizedString("The values to track the workout are invalid", comment: "WorkoutError")
        case .emptyResult:
            return NSLocalizedString("There is no results for this petition", comment: "WorkoutError")
        case .serviceError:
            return NSLocalizedString("There was an service error", comment: "WorkoutError")
        case .unknownServiceError:
            return NSLocalizedString("There was an unknown service error", comment: "WorkoutError")
        case .unknownError:
            return NSLocalizedString("There was an service error", comment: "WorkoutError")
        case .customError(let description):
            return NSLocalizedString(description, comment: "WorkoutError")
        }
    }
}

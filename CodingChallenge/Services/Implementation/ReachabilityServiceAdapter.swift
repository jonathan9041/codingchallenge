//
//  ReachabilityServiceAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/25/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
import Alamofire

final class ReachabilityServiceAdapter: ReachabilityService {
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}

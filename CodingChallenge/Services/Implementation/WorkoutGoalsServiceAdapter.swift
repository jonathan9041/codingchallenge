//
//  WorkoutGoalsServiceAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/21/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
import Alamofire

struct UnknownError: Error {}

final class WorkoutGoalsServiceAdapter: WorkoutGoalsService {
    struct Constants {
        static let workoutGoalsEndpoint = "https://thebigachallenge.appspot.com/_ah/api/myApi/v1/goals"
    }
    
    struct Dependencies {
        let urlSession: URLSessionProtocol = URLSessionAdapter()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies = .init()) {
        self.dependencies = dependencies
    }
    
    func getWorkoutGoals(completion: @escaping WorkoutGoalsResponse) {
        guard let request = makeRequest() else {
            completion(.failure(error: UnknownError()))
            return
        }
        dependencies.urlSession.performTask(with: request) { [weak self] (data, response, error) in
            guard let strongSelf = self else {
                completion(.failure(error: UnknownError()))
                return
            }
            
            strongSelf.handleResponse(data: data, error: error, completion: completion)
        }
    }
}

private extension WorkoutGoalsServiceAdapter {
    func makeRequest(with method: HTTPMethod = .get) -> URLRequest? {
        guard let url = URL(string: Constants.workoutGoalsEndpoint) else {
            return nil
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.cachePolicy = .reloadIgnoringCacheData
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request
    }
    
    func handleResponse(data: Data?, error: Error?, completion: @escaping WorkoutGoalsResponse) {
        guard let error = error else {
            guard let data = data else {
                completion(.failure(error: UnknownError()))
                return
            }
            
            let workoutGoals = parser(withData: data)
            
            completion(.success(response: workoutGoals))
            return
        }
        
        completion(.failure(error: error))
    }
    
    func parser(withData data: Data) -> [WorkoutGoal] {
        guard
            let jsonData = try? JSONSerialization.jsonObject(with: data, options: []),
            let data = jsonData as? [String: Any],
            let items = data["items"] as? [[String: Any]]
            else {
                return []
        }
        
        return items.compactMap { buildWorkoutGoal(json: $0) }
    }
    
    func buildWorkoutGoal(json: [String: Any]) -> WorkoutGoal? {
        guard let jData = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        else {
            return nil
        }
        
        do {
            let decoder = JSONDecoder()
            let exchangeCurrency = try decoder.decode(WorkoutGoal.self, from: jData)
            return exchangeCurrency
        } catch let error {
            print("build post error: \(error)")
            return nil
        }
    }
}

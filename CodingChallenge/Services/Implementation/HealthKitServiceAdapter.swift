//
//  HealthKitServiceAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation
import HealthKit

struct HealthkitSetupError {
    struct NotAvailableOnDevice: Error {}
    struct DataTypeNotAvailable: Error {}
}

class HealthKitServiceAdapter: HealthService {
    let healthStore = HKHealthStore()
    
    func authorizeInformationAccess(completion: @escaping HealthServiceResponse) {
        guard HKHealthStore.isHealthDataAvailable() else {
            completion(false, HealthkitSetupError.NotAvailableOnDevice())
          return
        }
        
        //TODO: Use characteristicType data such as date of birth, bood type, biologicalSex and bodyMassIndex
//        let dateOfBirth = HKObjectType.characteristicType(forIdentifier: .dateOfBirth),
//        let bloodType = HKObjectType.characteristicType(forIdentifier: .bloodType),
//        let biologicalSex = HKObjectType.characteristicType(forIdentifier: .biologicalSex),
//        let bodyMassIndex = HKObjectType.quantityType(forIdentifier: .bodyMassIndex),
//        let height = HKObjectType.quantityType(forIdentifier: .height),
//        let bodyMass = HKObjectType.quantityType(forIdentifier: .bodyMass),
        guard
            let activeEnergy = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned),
            let distance = HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning),
            let steps = HKObjectType.quantityType(forIdentifier: .stepCount),
            let heartRate = HKObjectType.quantityType(forIdentifier: .heartRate)
        else {
            completion(false, HealthkitSetupError.DataTypeNotAvailable())
            return
        }
        
        let healthKitTypesToWrite: Set<HKSampleType> = [activeEnergy,
                                                        HKObjectType.workoutType(),
                                                        distance,
                                                        steps]
            
        let healthKitTypesToRead: Set<HKObjectType> = [HKObjectType.workoutType(),
                                                       distance,
                                                       steps,
                                                       activeEnergy,
                                                       heartRate]
        
        healthStore.requestAuthorization(toShare: healthKitTypesToWrite,
                                             read: healthKitTypesToRead) { (success, error) in
          completion(success, error)
        }
    }
    
    func getRunningWorkoutGoalsProgress(completion: @escaping WorkoutGoalsProgressResponse) {
        getWorkoutActivity(for: .running, completion: completion)
    }
    
    func getWalkingWorkoutGoalsProgress(completion: @escaping WorkoutGoalsProgressResponse) {
        getWorkoutActivity(for: .walking, completion: completion)
    }
    
    func getStepWorkoutGoalsProgress(completion: @escaping WorkoutGoalsProgressResponse) {
        getStepWorkoutActivity(completion: completion)
    }
    
    func getHearRateAverageForToday(completion: @escaping DailyHeartRateAverageResponse) {
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let today = cal.startOfDay(for: date)
        
        let predicate = HKQuery.predicateForSamples(withStart: today, end: Date(), options: .strictStartDate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate,
                                              ascending: true)
        
        let query = HKSampleQuery(sampleType: HKQuantityType.quantityType(forIdentifier: .heartRate)!,
                                  predicate: predicate,
                                  limit: 0,
                                  sortDescriptors: [sortDescriptor]) { [weak self] (query, samples, error) in
                                    DispatchQueue.main.async {
                                        guard
                                            error == nil,
                                            let samples = samples
                                        else {
                                            completion(nil, error)
                                            return
                                        }
                                        
                                        let heartRateAverage = samples.compactMap { self?.handleHeartRateResult(sample: $0) }
                                        
                                        completion(heartRateAverage, nil)
                                    }
                                }

        healthStore.execute(query)
    }
    
    func trackWalkingRunningWorkout(by startDate: Date, endDate: Date, type: WorkoutType, distance: Double, completion: @escaping HealthServiceResponse) {
        let workoutType: HKWorkoutActivityType = type == .running ? .running : .walking
        let workoutConfiguration = HKWorkoutConfiguration()
        workoutConfiguration.activityType = workoutType
        workoutConfiguration.locationType = .outdoor
        
        let builder = HKWorkoutBuilder(healthStore: healthStore,
                                       configuration: workoutConfiguration,
                                       device: .local())
            
        builder.beginCollection(withStart: startDate) { (success, error) in
          guard success else {
            completion(false, error)
            return
          }
        }
        
        guard let quantityType = HKQuantityType.quantityType(
          forIdentifier: .distanceWalkingRunning) else {
            completion(false, nil)
            return
        }
            
        let unit: HKUnit = .meter()
        let quantity = HKQuantity(unit: unit,
                                  doubleValue: distance)
        
        let sample = HKCumulativeQuantitySample(type: quantityType,
                                                quantity: quantity,
                                                start: startDate,
                                                end: endDate)
        
        builder.add([sample]) { (success, error) in
            guard success else {
                completion(false, error)
            return
            }
              
            builder.endCollection(withEnd: endDate) { (success, error) in
                guard success else {
                  completion(false, error)
                  return
                }
                    
                builder.finishWorkout { (_, error) in
                  let success = error == nil
                  completion(success, error)
                }
            }
        }
    }
    
    func trackStepWorkout(by startDate: Date, endDate: Date, steps: Int, completion: @escaping HealthServiceResponse) {
        let workoutConfiguration = HKWorkoutConfiguration()
        workoutConfiguration.activityType = .stepTraining
        workoutConfiguration.locationType = .outdoor
        
        let builder = HKWorkoutBuilder(healthStore: healthStore,
                                       configuration: workoutConfiguration,
                                       device: .local())
            
        builder.beginCollection(withStart: startDate) { (success, error) in
          guard success else {
            completion(false, error)
            return
          }
        }
        
        guard let quantityType = HKQuantityType.quantityType(
          forIdentifier: .stepCount) else {
            completion(false, nil)
            return
        }
            
        let unit: HKUnit = .count()
        let quantity = HKQuantity(unit: unit,
                                  doubleValue: Double(steps))
        
        let sample = HKCumulativeQuantitySample(type: quantityType,
                                                quantity: quantity,
                                                start: startDate,
                                                end: endDate)
        
        builder.add([sample]) { (success, error) in
            guard success else {
                completion(false, error)
            return
            }
              
            builder.endCollection(withEnd: endDate) { (success, error) in
                guard success else {
                  completion(false, error)
                  return
                }
                    
                builder.finishWorkout { (_, error) in
                  let success = error == nil
                  completion(success, error)
                }
            }
        }
    }
}

private extension HealthKitServiceAdapter {
    func handleHeartRateResult(sample: HKSample) -> HeartRateData? {
        guard let heartRateResult = sample as? HKQuantitySample else {
            return nil
        }
        
        let heartRateUnit: HKUnit = HKUnit(from: "count/min")
        let heartRate = heartRateResult.quantity.doubleValue(for: heartRateUnit)
        let startDate = heartRateResult.startDate
        let endDate = heartRateResult.endDate
        
        return HeartRateData(bpm: heartRate,
                             startDate: startDate,
                             endDate: endDate,
                             quantityType: "")
    }
    func createRunningWorkoutGoalProgress(workout: HKWorkout) -> WorkoutGoalProgress? {
        let distance = workout.totalDistance?.doubleValue(for: .meter())
        let activeEnergy = workout.totalEnergyBurned?.doubleValue(for: .kilocalorie())
        return RunningWorkoutGoalProgress(startDate: workout.startDate,
                                          endDate: workout.endDate,
                                          distance: distance,
                                          activeEnergy: activeEnergy)
    }
    
    func createWalkingWorkoutGoalProgress(workout: HKWorkout) -> WorkoutGoalProgress? {
        let distance = workout.totalDistance?.doubleValue(for: .meter())
        let activeEnergy = workout.totalEnergyBurned?.doubleValue(for: .kilocalorie())
        return WalkingWorkoutGoalProgress(startDate: workout.startDate,
                                          endDate: workout.endDate,
                                          distance: distance,
                                          activeEnergy: activeEnergy)
    }
    
    func createStepsWorkoutGoalProgress(steps: Double, startDate: Date, endDate: Date) -> WorkoutGoalProgress? {
        return StepWorkoutGoalProgress(startDate: startDate, endDate: endDate, steps: Int(steps))
    }
    
    func createWalkingRunningWorkoutGoalProgress(type: HKWorkoutActivityType, workout: HKWorkout) -> WorkoutGoalProgress? {
        switch type {
        case .running:
            return createRunningWorkoutGoalProgress(workout: workout)
        case .walking:
            return createWalkingWorkoutGoalProgress(workout: workout)
        default:
            return nil
        }
    }
    
    func getWorkoutActivity(for type: HKWorkoutActivityType, completion: @escaping WorkoutGoalsProgressResponse) {
        let workoutPredicate = HKQuery.predicateForWorkouts(with: type)
        
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let today = cal.startOfDay(for: date)
        
        let predicate = HKQuery.predicateForSamples(withStart: today, end: Date(), options: .strictStartDate)
        var interval = DateComponents()
        interval.day = 1
        
        let compound = NSCompoundPredicate(andPredicateWithSubpredicates:
          [workoutPredicate, predicate])
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate,
                                              ascending: true)
        
        let query = HKSampleQuery(sampleType: .workoutType(),
                                  predicate: compound,
                                  limit: 0,
                                  sortDescriptors: [sortDescriptor]) { [weak self] (query, samples, error) in
                                    DispatchQueue.main.async {
                                        guard
                                            let workouts = samples as? [HKWorkout],
                                            error == nil
                                        else {
                                          completion(nil, [error])
                                          return
                                        }
                                        let workoutGoalsProgress = workouts.compactMap { self?.createWalkingRunningWorkoutGoalProgress(type: type, workout: $0) }
                                        completion(workoutGoalsProgress, [])
                                    }
                                }

        healthStore.execute(query)
    }
    
    func getStepWorkoutActivity(completion: @escaping WorkoutGoalsProgressResponse) {
        let stepsCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)

        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let today = cal.startOfDay(for: date)
        let endDate = Date()

        let predicate = HKQuery.predicateForSamples(withStart: today, end: endDate, options: .strictStartDate)
        var interval = DateComponents()
        interval.day = 1

        let query = HKStatisticsCollectionQuery(quantityType: stepsCount!,
                                                quantitySamplePredicate: predicate,
                                                options: [.cumulativeSum],
                                                anchorDate: today as Date,
                                                intervalComponents: interval)

        query.initialResultsHandler = { [weak self] query, results, error in
            guard error == nil else {
                completion(nil, [error])
                return
            }

            guard let myResults = results else {
                completion(nil, [WorkoutError.customError(description: "The step workout query result is empty")])
                return
            }
            
            myResults.enumerateStatistics(from: today, to: endDate) {
                statistics, stop in

                guard let quantity = statistics.sumQuantity() else {
                    completion(nil, [WorkoutError.customError(description: "There is an error getting the step workout quantity")])
                    return
                }

                let steps = quantity.doubleValue(for: HKUnit.count())
                guard let stepWorkout = self?.createStepsWorkoutGoalProgress(steps: steps, startDate: today, endDate: endDate) else {
                    completion(nil, [WorkoutError.emptyResult])
                    return
                }

                completion([stepWorkout], [])
            }
        }

        healthStore.execute(query)
    }
}

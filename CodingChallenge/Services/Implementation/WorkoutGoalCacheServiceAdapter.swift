//
//  WorkoutGoalCacheServiceAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/24/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

final class WorkoutGoalCacheServiceAdapter: WorkoutGoalCacheService {
    let defaults = UserDefaults.standard
    
    func saveWorkoutGoals(workoutGoals: [WorkoutGoal]) {
        do  {
            let encodedWorkoutGoals = try JSONEncoder().encode(workoutGoals)
            
            defaults.set(encodedWorkoutGoals, forKey: "workoutsGoals")
        } catch {
          print("Error to save the workout goals")
        }
    }
    
    func retrieveWorkoutGoals() -> [WorkoutGoal]? {
        do {
            guard let data = defaults.object(forKey: "workoutsGoals") as? Data
            else {
                return nil
            }
             
            let workoutGoals = try JSONDecoder().decode([WorkoutGoal].self, from: data)
            
            return workoutGoals        
        } catch {
            return nil
        }
    }
}

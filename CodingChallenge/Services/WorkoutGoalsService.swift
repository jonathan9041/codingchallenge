//
//  WorkoutGoalsService.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/21/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

typealias WorkoutGoalsResponse = (ServiceResponse<[WorkoutGoal]>) -> Void

protocol WorkoutGoalsService {
    func getWorkoutGoals(completion: @escaping WorkoutGoalsResponse)
}

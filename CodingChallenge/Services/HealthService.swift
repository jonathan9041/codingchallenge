//
//  HealthService.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

typealias HealthServiceResponse = ((Bool, Error?) -> Void)
typealias WorkoutGoalsProgressResponse = (([WorkoutGoalProgress]?, [Error?]) -> Void)
typealias DailyHeartRateAverageResponse = (([HeartRateData]?, Error?) -> Void)

protocol HealthService {
    func authorizeInformationAccess(completion: @escaping HealthServiceResponse)
    func getRunningWorkoutGoalsProgress(completion: @escaping WorkoutGoalsProgressResponse)
    func getWalkingWorkoutGoalsProgress(completion: @escaping WorkoutGoalsProgressResponse)
    func getStepWorkoutGoalsProgress(completion: @escaping WorkoutGoalsProgressResponse)
    func getHearRateAverageForToday(completion: @escaping DailyHeartRateAverageResponse)
    func trackWalkingRunningWorkout(by date: Date, endDate: Date, type: WorkoutType, distance: Double, completion: @escaping HealthServiceResponse)
    func trackStepWorkout(by startDate: Date, endDate: Date, steps: Int, completion: @escaping HealthServiceResponse)
}

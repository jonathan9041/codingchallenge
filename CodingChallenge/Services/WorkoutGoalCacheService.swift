//
//  WorkoutGoalCacheService.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/24/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol WorkoutGoalCacheService {
    func saveWorkoutGoals(workoutGoals: [WorkoutGoal])
    func retrieveWorkoutGoals() -> [WorkoutGoal]?
}

//
//  ReachabilityService.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/25/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol ReachabilityService {
    var isConnectedToInternet: Bool { get }
}

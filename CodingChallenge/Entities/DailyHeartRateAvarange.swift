//
//  DailyHeartRateAvarange.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/26/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

struct DailyHeartRateAverage {
    var bpm: Double
}

struct HeartRateData {
    var bpm: Double
    var startDate: Date
    var endDate: Date
    var quantityType: String
}

//
//  WorkoutGoalProgress.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol WorkoutGoalProgress {
    var startDate: Date { set get }
    var endDate: Date { set get }
    
    func accept<V: WorkoutGoalProgressVisitor>(visitor: V, additionalInfo: V.Info) -> V.Result
}

extension WorkoutGoalProgress {
    func accept<V>(visitor: V, additionalInfo: V.Info) -> V.Result where V : WorkoutGoalProgressVisitor {
        assertionFailure("Implement me in a subclass")
        return visitor.visit(workoutGoalProgress: self, additionalInfo: additionalInfo)
    }
}

struct RunningWorkoutGoalProgress: WorkoutGoalProgress {
    var startDate: Date
    var endDate: Date
    var distance: Double?
    var activeEnergy: Double?
}

extension RunningWorkoutGoalProgress {
    func accept<V>(visitor: V, additionalInfo: V.Info) -> V.Result where V : WorkoutGoalProgressVisitor {
        return visitor.visit(runningWorkoutGoalProgress: self, additionalInfo: additionalInfo)
    }
}

struct WalkingWorkoutGoalProgress: WorkoutGoalProgress {
    var startDate: Date
    var endDate: Date
    var distance: Double?
    var activeEnergy: Double?
}

extension WalkingWorkoutGoalProgress {
    func accept<V>(visitor: V, additionalInfo: V.Info) -> V.Result where V : WorkoutGoalProgressVisitor {
        return visitor.visit(walkingWorkoutGoalProgress: self, additionalInfo: additionalInfo)
    }
}

struct StepWorkoutGoalProgress: WorkoutGoalProgress {
    var startDate: Date
    var endDate: Date
    var steps: Int?
}

extension StepWorkoutGoalProgress {
    func accept<V>(visitor: V, additionalInfo: V.Info) -> V.Result where V : WorkoutGoalProgressVisitor {
        visitor.visit(stepWorkoutGoalProgress: self, additionalInfo: additionalInfo)
    }
}

protocol WorkoutGoalProgressVisitor {
    associatedtype Result
    associatedtype Info
    
    func visit(runningWorkoutGoalProgress: RunningWorkoutGoalProgress, additionalInfo: Info) -> Result
    func visit(walkingWorkoutGoalProgress: WalkingWorkoutGoalProgress, additionalInfo: Info) -> Result
    func visit(stepWorkoutGoalProgress: StepWorkoutGoalProgress, additionalInfo: Info) -> Result
}

extension WorkoutGoalProgressVisitor {
    func visit(workoutGoalProgress: WorkoutGoalProgress, additionalInfo: Info) -> Result {
        return workoutGoalProgress.accept(visitor: self, additionalInfo: additionalInfo)
    }
}

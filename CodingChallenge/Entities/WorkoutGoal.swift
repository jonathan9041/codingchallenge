//
//  WorkoutGoal.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/21/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

enum WorkoutType: String {
    case walking
    case running
    case step
    case unknown
    
    init(type: String) {
        switch type {
        case "step":
            self = .step
        case "walking_distance":
            self = .walking
        case "running_distance":
            self = .running
        default:
            self = .unknown
        }
    }
}

enum Medal {
    case bronze
    case silver
    case gold
    case zombie
    case none
    
    init(type: String) {
        switch type {
        case "bronze_medal":
            self = .bronze
        case "silver_medal":
            self = .silver
        case "gold_medal":
            self = .gold
        case "zombie_hand":
            self = .zombie
        default:
            self = .none
        }
    }
}

struct WorkoutGoal: Codable, Equatable {
    var id: String
    var title: String
    var description: String
    var type: String
    var goal: Double,
    reward: Reward
    
    struct Reward: Codable, Equatable {
        var trophy: String
        var points: Int
    }
}

extension WorkoutGoal {
    var workoutType: WorkoutType {
        return WorkoutType(type: self.type)
    }
    
    var medal: Medal {
        return Medal(type: self.reward.trophy)
    }
    
    var goalText: String {
        switch workoutType {
        case .running, .walking:
            return formatGoalForWalkingRunning()
        case .step:
            return formatGoalForStepWorkout()
        default:
            return ""
        }
    }
    
    func formatGoalForStepWorkout() -> String {
        let unit = "steps"
        let goalFormated: Double = goal >= 1000 ? goal / 1000 : goal
        let formatString = goal >= 1000 ? "%.0fk %@" : "%.0f %@"
        return String(format: formatString, goalFormated, unit)
    }
    
    func formatGoalForWalkingRunning() -> String {
        var unit = ""
        var goalFormated: Double = 0
        
        if goal >= 1000 {
            unit = "km"
            goalFormated = goal > 0 ? goal / 1000 : 0
        } else {
            unit = "meters"
            goalFormated = goal
        }
        
        let formatString = goalFormated.truncatingRemainder(dividingBy: 1) == 0 ? "%.0f %@" : "%.1f %@"
        
        return String(format: formatString, goalFormated, unit)
    }
}

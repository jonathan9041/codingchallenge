//
//  GetWorkoutGoals.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/21/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol GetWorkoutGoals {
    func execute(completion: @escaping WorkoutGoalsResponse)
}

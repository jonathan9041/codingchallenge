//
//  TrackWalkingRunningWorkout.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol TrackWalkingRunningWorkout {
    func execute(with type: WorkoutType, distance: Double, duration: TimeInterval, completion: @escaping HealthServiceResponse)
}

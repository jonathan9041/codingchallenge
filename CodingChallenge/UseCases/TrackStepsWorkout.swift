//
//  TrackStepsWorkout.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/23/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol TrackStepsWorkout {
    func execute(steps: Int, duration: TimeInterval, completion: @escaping HealthServiceResponse)
}

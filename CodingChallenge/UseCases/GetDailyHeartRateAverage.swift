//
//  GetDailyHeartRateAverage.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/26/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

protocol GetDailyHeartRateAverage {
    func execute(completion: @escaping DailyHeartRateAverageResponse) 
}

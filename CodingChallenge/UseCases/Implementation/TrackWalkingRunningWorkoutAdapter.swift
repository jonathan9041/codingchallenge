//
//  TrackWalkingRunningWorkoutAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

final class TrackWalkingRunningWorkoutAdapter: TrackWalkingRunningWorkout {
    struct Dependencies {
        let healthService: HealthService = HealthKitServiceAdapter()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies = .init()) {
        self.dependencies = dependencies
    }
    
    func execute(with type: WorkoutType, distance: Double, duration: TimeInterval, completion: @escaping HealthServiceResponse) {
        guard
            distance > 0,
            duration > 0
        else {
            completion(false, WorkoutError.customError(description: "The parameters to track the workout are invalid"))
            return
        }
        
        let endDate = Date()
        let startDate = endDate.addingTimeInterval(duration * -60)
        dependencies.healthService.trackWalkingRunningWorkout(by: startDate, endDate: endDate, type: type, distance: distance, completion: completion)
    }
}

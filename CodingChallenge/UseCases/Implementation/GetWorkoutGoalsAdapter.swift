//
//  GetWorkoutGoalsAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/21/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

final class GetWorkoutGoalsAdapter: GetWorkoutGoals {
    struct Dependencies {
        var workoutGoalsService: WorkoutGoalsService = WorkoutGoalsServiceAdapter()
        var workoutGoalCache: WorkoutGoalCacheService = WorkoutGoalCacheServiceAdapter()
        var reachabilityService: ReachabilityService = ReachabilityServiceAdapter()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies = .init()) {
        self.dependencies = dependencies
    }
    
    func execute(completion: @escaping WorkoutGoalsResponse) {
        if dependencies.reachabilityService.isConnectedToInternet {
            dependencies.workoutGoalsService.getWorkoutGoals { [weak self] (workoutGoalsServiceResponse) in
                switch workoutGoalsServiceResponse {
                case .success(let workoutGoals):
                    self?.dependencies.workoutGoalCache.saveWorkoutGoals(workoutGoals: workoutGoals)
                    completion(.success(response: workoutGoals))
                case .failure(let error):
                    completion(.failure(error: error))
                }
            }
        } else {
            guard let workoutGoals = dependencies.workoutGoalCache.retrieveWorkoutGoals() else {
                completion(.failure(error: WorkoutError.customError(description: "There is no workout goals saved")))
                return
            }
            
            completion(.success(response: workoutGoals))
        }
    }
}

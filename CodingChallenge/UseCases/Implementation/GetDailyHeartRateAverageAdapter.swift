//
//  GetDailyHeartRateAverageAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/26/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

final class GetDailyHeartRateAverageAdapter: GetDailyHeartRateAverage {
    struct Depedencies {
        var healthService: HealthService = HealthKitServiceAdapter()
    }
    
    let dependencies: Depedencies
    
    init(dependencies: Depedencies = .init()) {
        self.dependencies = dependencies
    }
    
    func execute(completion: @escaping DailyHeartRateAverageResponse) {
        dependencies.healthService.getHearRateAverageForToday(completion: completion)
    }
}

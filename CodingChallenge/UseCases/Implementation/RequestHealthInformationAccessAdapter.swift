//
//  RequestHealthInformationAccessAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

final class RequestHealthInformationAccessAdapter: RequestHealthInformationAccess {
    struct Dependencies {
        let healthService: HealthService = HealthKitServiceAdapter()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies = .init()) {
        self.dependencies = dependencies
    }
    
    func execute(completion: @escaping HealthServiceResponse) {
        dependencies.healthService.authorizeInformationAccess(completion: completion)
    }
}

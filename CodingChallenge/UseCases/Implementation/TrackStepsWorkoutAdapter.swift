//
//  TrackStepsWorkoutAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/23/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

final class TrackStepsWorkoutAdapter: TrackStepsWorkout {
    struct Dependencies {
        let healthService: HealthService = HealthKitServiceAdapter()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies = .init()) {
        self.dependencies = dependencies
    }
    
    func execute(steps: Int, duration: TimeInterval, completion: @escaping HealthServiceResponse) {
        guard
            duration > 0,
            steps > 0
        else {
            completion(false, WorkoutError.customError(description: "The parameters to track the workout are invalid"))
            return
        }
        
        let endDate = Date()
        let startDate = endDate.addingTimeInterval(duration * -60)
        dependencies.healthService.trackStepWorkout(by: startDate, endDate: endDate, steps: steps, completion: completion)
    }
}

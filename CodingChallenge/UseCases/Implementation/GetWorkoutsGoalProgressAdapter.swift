//
//  GetWorkoutGoalsProgressAdapter.swift
//  CodingChallenge
//
//  Created by Jonathand Alberto Serrano Serrano on 3/22/20.
//  Copyright © 2020 Jonathand Alberto Serrano Serrano. All rights reserved.
//

import Foundation

final class GetWorkoutGoalsProgressAdapter: GetWorkoutsGoalsProgress {
    struct Dependencies {
        let healthService: HealthService = HealthKitServiceAdapter()
    }
    
    let dependencies: Dependencies
    var workoutGoalProgress: [WorkoutGoalProgress] = []
    var errors: [Error?] = []
    
    init(dependencies: Dependencies = .init()) {
        self.dependencies = dependencies
    }
    
    func execute(completion: @escaping WorkoutGoalsProgressResponse) {
        handleWorkGoalProgressAsyncOperation(completion: completion)
    }
}

private extension GetWorkoutGoalsProgressAdapter {
    func handleWorkGoalProgressAsyncOperation(completion: @escaping WorkoutGoalsProgressResponse) {
        workoutGoalProgress = []
        errors = []
        let serialQueue = DispatchQueue(label: "serial.queue")
        let group = DispatchGroup()
        group.enter()
        
        getStepsWorkoutGoalsProgress(with: serialQueue, group: group)
        getRunningWorkoutGoalsProgress(with: serialQueue, group: group)
        getWalkingWorkoutGoalsProgress(with: serialQueue, group: group)
        handleWorkoutGoalsProgress(with: serialQueue, group: group, completion: completion)
    }
    
    func getStepsWorkoutGoalsProgress(with serialQueue: DispatchQueue, group: DispatchGroup) {
        serialQueue.async { [weak self] in
            guard let strongSelf = self else {
                group.leave()
                return
            }
            
            strongSelf.dependencies.healthService.getStepWorkoutGoalsProgress { (stepsWorkoutGoalProgress, errorsResponse) in
                guard
                    errorsResponse.isEmpty,
                    let stepsWorkoutGoalProgress = stepsWorkoutGoalProgress
                else {
                    self?.errors.append(contentsOf: errorsResponse)
                    group.leave()
                    return
                }
                
                strongSelf.workoutGoalProgress.append(contentsOf: stepsWorkoutGoalProgress)
                group.leave()
            }
        }
    }
    
    func getRunningWorkoutGoalsProgress(with serialQueue: DispatchQueue, group: DispatchGroup) {
        serialQueue.async { [weak self] in
            group.wait()
            group.enter()
            
            guard let strongSelf = self else {
                group.leave()
                return
            }
            
            strongSelf.dependencies.healthService.getRunningWorkoutGoalsProgress { (runningWorkoutGoalProgress, errorsResponse) in
                guard
                    errorsResponse.isEmpty,
                    let runningWorkoutGoalProgress = runningWorkoutGoalProgress
                else {
                    strongSelf.errors.append(contentsOf: errorsResponse)
                    group.leave()
                    return
                }
                
                strongSelf.workoutGoalProgress.append(contentsOf: runningWorkoutGoalProgress)
                group.leave()
            }
        }
    }
    
    func getWalkingWorkoutGoalsProgress(with serialQueue: DispatchQueue, group: DispatchGroup) {
        serialQueue.async { [weak self] in
            group.wait()
            group.enter()
            
            guard let strongSelf = self else {
                group.leave()
                return
            }
            
            strongSelf.dependencies.healthService.getWalkingWorkoutGoalsProgress { (walkingWorkoutGoalProgress, errorsReponse) in
                guard
                    errorsReponse.isEmpty,
                    let walkingWorkoutGoalProgress = walkingWorkoutGoalProgress
                else {
                    strongSelf.errors.append(contentsOf: errorsReponse)
                    group.leave()
                    return
                }
                
                strongSelf.workoutGoalProgress.append(contentsOf: walkingWorkoutGoalProgress)
                group.leave()
            }
        }
    }
    
    func handleWorkoutGoalsProgress(with serialQueue: DispatchQueue, group: DispatchGroup, completion: @escaping WorkoutGoalsProgressResponse){
        serialQueue.async { [weak self] in
            group.wait()
            group.enter()
            
            guard let strongSelf = self else {
                group.leave()
                return
            }
            
            group.leave()
            completion(strongSelf.workoutGoalProgress, strongSelf.errors)
        }
    }
}

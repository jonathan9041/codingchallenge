# iOS Coding Challenge - Jonathand Alberto Serrano Serrano

#### Architecture

This test project is using Clean Architecture. This test has been divided in a couple of layers: Services, Use cases and VIP cycle (Interactor, Presenter and View)

#### Best Practices

This project has been implemented using a design pattern called visitor pattern, specifically on the WorkoutGoalsProgressPresenter class where there are multiple workoutGoalProgress (Walking, running and step) that conform a parent protocol called WorkoutGoalProgress, in this case, the visitor pattern helps to implement a specific method for each kind of workout goal progress polymorphically.

Also, Each layer has a unique responsibility and each layer handles a specific logic for each functionality.

#### External Libraries
With Carthage. This Test is using an external library called Alamofire  [https://github.com/Alamofire/Alamofire].

#### Unit Test
This test project implements a unit test on the use case layer, this is an introduction to create more unit tests in the rest of the architecture layer. Those tests are using mocks that conform to a specific protocol to modify the behavior of the methods or variables.

#### Reachability
This test has reachability support to check if the app is connected to the internet. When the app is connected, the app calls the workout goals endpoint. When the app is disconnected to the internet the app retrieves the workout goals saved from the User defaults.

#### Additional Information

### Main features:

1. Retrieve the workout goals from the endpoint and user defaults when the app is disconnected to the internet. Daily workout goals screen.
2. Retrieve the workout progress from HealthKIt using the Health Kit API, that the user did in the current day. This workout is classified into 3 types: Walking, Running and Step. The App shows the different types and shoe the specific information about each workout such as distance, active energy, number of steps, daily heart rate average, the total points earned by workout activity according to with the daily workout goals, current workout achieved, and next goal to archive and the points to earn.
3. Tracking workouts: The app can track 3 different workout types: Walking, Running and Step activity. The app catches the distance, and the duration of the workout, for the step activity the app capture the number of steps and the duration of the workout to save this information into HealthKit using the Health Kit API.

* Screen Orientation: Portrait

Marzo 26 2020.
